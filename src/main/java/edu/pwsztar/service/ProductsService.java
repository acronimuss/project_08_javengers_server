package edu.pwsztar.service;

import edu.pwsztar.domain.dto.ProductDto;

import java.io.IOException;

/**
 * Interfejs pozwalający na operacje związane z odczytem produktów
 */
public interface ProductsService{
    public ProductDto[] getProducts() throws IOException;

    public ProductDto[] getProducts(String json) throws Exception;

    public ProductDto findById(int id) throws IOException;
}
