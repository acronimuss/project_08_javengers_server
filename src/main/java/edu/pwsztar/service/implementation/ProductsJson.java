package edu.pwsztar.service.implementation;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.pwsztar.domain.dto.ProductDto;
import edu.pwsztar.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Ta klasa pozwala na operacje związane z listą produktów w postaci JSON
 */
@Component
public class ProductsJson implements ProductsService{
    /**
     * Plik JSON w którym przechowywane są informacje o produktach
     */
    @Value("classpath:products.json")
    Resource usersFile;

    /**
     * Obiekt wykorzystywany przy konwersji do oraz z JSON
     */
    @Autowired
    ObjectMapper mapper;

    /**
     * Funkcja pobierająca listę produktów z pliku
     * @return Tablica obiektów klasy ProductDto
     * @throws IOException W przypadku błędów w odczycie
     */
    @Override
    public ProductDto[] getProducts() throws IOException {

        return mapper.readValue(usersFile.getFile(), ProductDto[].class);
    }

    /**
     * Ta funkcja pozwala na konwersję Stringu JSON na tablicę obiektów ProductDto
     * @param json String zawierający obiekty ProductDto w postaci JSON
     * @return Tablica obiektów klasy ProductDto
     * @throws Exception W przypadku błędów konwersji
     */
    @Override
    public ProductDto[] getProducts(String json) throws Exception {

        return mapper.readValue(json, ProductDto[].class);
    }

    /**
     * Ta funkcja wyszukuje w pliku obiekt o danym ID
     * @param id id produktu
     * @return Obiekt ProductDto lub null, gdy obiekt nie zostanie odnaleziony
     * @throws IOException W przypadku błędów w odczycie
     */
    @Override
    public ProductDto findById(int id) throws IOException {

        ProductDto[] products = mapper.readValue(usersFile.getFile(), ProductDto[].class);


        for(ProductDto p : products){
            if(p.getId()==id)
                return p;
        }

        return null;
    }


}
