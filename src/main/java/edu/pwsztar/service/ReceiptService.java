package edu.pwsztar.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import edu.pwsztar.domain.dto.ProductDto;

import java.io.FileOutputStream;
import java.time.ZonedDateTime;


/**
 * Klasa pozwalająca na generowanie paragonów z zamówienia
 */
public class ReceiptService {

    private Font bigFont = new Font(Font.FontFamily.TIMES_ROMAN, 25.0f, Font.BOLD, BaseColor.BLACK);
    private Font smallFont = new Font(Font.FontFamily.TIMES_ROMAN, 13.0f, Font.NORMAL, BaseColor.BLACK);

    /**
     * Funkcja generująca paragon w postaci pliku PDF
     * @param products tablica obiektów klasy ProductDto
     * @return String z nazwą utworzonego pliku
     */
    public String getReceiptFile(ProductDto[] products) throws Exception{


        String filename = "paragon"+ "_" + ZonedDateTime.now().toEpochSecond() + ".pdf";

        Document document = new Document();

        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));

        document.open();

        document.addTitle("Zapraszamy ponownie!");





        Paragraph title = new Paragraph(new Chunk("Paragon", bigFont));
        title.setAlignment(Element.ALIGN_CENTER);
        title.setSpacingAfter(10);
        document.add(title);

        double fullPrice = 0;


        PdfPTable table = new PdfPTable(4);
        table.setWidths(new int[]{3, 1, 1, 1});
        table.addCell(newCell("Nazwa produktu"));
        table.addCell(newCell("Ilosc"));
        table.addCell(newCell("Cena"));
        table.addCell(newCell("Suma"));


        for(ProductDto product : products){

            fullPrice+=product.getPrice()*product.getAmount();

            table.addCell(newCell(product.getName()));
            table.addCell(newCell(String.valueOf(product.getAmount())));
            table.addCell(newCell(String.format("%.2f",product.getPrice())+"PLN"));
            table.addCell(newCell( String.format("%.2f",product.getPrice()*product.getAmount())+"PLN"));
        }

        document.add(table);



        Chunk c;
        if(fullPrice>100.0)
            c = new Chunk("Suma: "+String.format("%.2f", (fullPrice-((fullPrice/100.0)*20.0)))+"PLN (Po obnizce)", bigFont);

        else
            c = new Chunk("Suma: "+String.format("%.2f", fullPrice)+"PLN", bigFont);


        Paragraph sum = new Paragraph(c);
        sum.setAlignment(Element.ALIGN_CENTER);
        sum.setSpacingBefore(10);
        document.add(sum);




        document.close();


        return filename;
    }

    /**
     * Tworzy domyślną komórkę tabeli
     * @param content Napis wyświetlany w komórce
     * @return Obiekt klasy PdfPCell
     */
    private PdfPCell newCell(String content) {
        PdfPCell cell = new PdfPCell(new Phrase(content, smallFont));
        cell.setBorderWidth(0);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setPadding(5);

        return cell;
    }

}
