package edu.pwsztar.domain.dto;

/**
 * Klasa obiektów przekazywanych z i do serwera
 */
public class ProductDto {

    /**
     * ID produktu
     */
    private int id;
    /**
     * Nazwa produktu
     */
    private String name;
    /**
     * Typ produktu, np. (Pizza, Napój)
     */
    private String type;
    /**
     *
     * Słowny opis produktu
     */
    private String description;
    /**
     * Cena jednej sztuki tego produktu
     */
    private double price;
    /**
     * Ilość sztuk tego produktu(Wykorzystywane przy zamówieniu)
     */
    private int amount;
    /**
     * Wykorzystywane przy tworzeniu pizzy 50/50, ID pierwszego typu pizzy
     */
    private int optional1;
    /**
     * Wykorzystywane przy tworzeniu pizzy 50/50, ID drugiego typu pizzy
     */
    private int optional2;

    /**
     * Domyślny konstruktor
     */
    public ProductDto(){

    }

    /**
     * Konstruktor pozwalający tworzyć własny produkt
     * @param id Id produktu
     * @param name Nazwa produktu
     * @param type Typ produktu
     * @param description Opis produktu
     * @param price Cena jednej sztuki produktu
     * @param amount Ilość sztuk tego produktu
     * @param optional1 ID pierwszego typu pizzy (przy tworzeniu pizzy 50/50)
     * @param optional2 ID drugiego typu pizzy (przy tworzeniu pizzy 50/50)
     */
    public ProductDto(int id, String name, String type, String description, double price, int amount, int optional1, int optional2){
        this.id = id;
        this.name = name;
        this.type = type;
        this.description = description;
        this.price = price;
        this.amount = amount;
        this.optional1 = optional1;
        this.optional2 = optional2;
    }

    /**
     * Domyślny getter
     * @return Zwraca nazwę produktu
     */
    public String getName() {
        return name;
    }
    /**
     * Domyślny getter
     * @return Zwraca opis produktu
     */
    public String getDescription() {
        return description;
    }
    /**
     * Domyślny getter
     * @return Zwraca cenę produktu
     */
    public double getPrice() {
        return price;
    }
    /**
     * Domyślny getter
     * @return Zwraca typ produktu
     */
    public String getType() {
        return type;
    }
    /**
     * Domyślny getter
     * @return Zwraca ilość
     */
    public int getAmount() {
        return amount;
    }
    /**
     * Domyślny getter
     * @return Zwraca ID produktu
     */
    public int getId() {
        return id;
    }
    /**
     * Domyślny getter
     * @return Zwraca potencjalne ID produktu typu pizza
     */
    public int getOptional1() {
        return optional1;
    }
    /**
     * Domyślny getter
     * @return Zwraca potencjalne ID produktu typu pizza
     */
    public int getOptional2() {
        return optional2;
    }

    /**
     * Domyślny setter, pozwala na zmianę ilości zamawianego produktu
     * @param amount Ilość
     */

    public void setAmount(int amount){
        this.amount = amount;
    }

}
