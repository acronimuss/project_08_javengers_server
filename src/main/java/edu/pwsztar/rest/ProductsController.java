package edu.pwsztar.rest;

import edu.pwsztar.domain.dto.ProductDto;
import edu.pwsztar.service.ReceiptService;
import edu.pwsztar.service.implementation.ProductsJson;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileInputStream;
import java.io.IOException;


/**
 * Kontroler obsługujący komunikację z aplikacją kliencką
 */
@Controller
@RequestMapping(value = "/produkty")
public class ProductsController {

    /**
     * Obiekt klasy ProductsJson pozwalający na odczyt produktów z pliku
     */
    @Autowired
    ProductsJson productsModel;

    /**
     * Obiekt klasy ReceiptService pozwalający na wygenerowanie paragonu
     */
    ReceiptService service = new ReceiptService();

    /**
     * Funkcja obsługująca żądanie pobrania listy produktów przez klienta
     * @return tablica obiektów ProductDto jako JSON
     */
    @GetMapping(value="/pobierz")
    public ResponseEntity<ProductDto[]> sendProducts() {

        try {

            ProductDto[] produkty = productsModel.getProducts();

            return new ResponseEntity<>(produkty, HttpStatus.OK);
        }

        catch(IOException e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    /**
     * Funkcja realizująca żądanie wykonania zamówienia z klienta
     * @return zawartosć pliku PDF (paragonu) jako tablica bajtów
     */
    @PostMapping(value="/zamow")
    public ResponseEntity<byte[]> getOrder(@RequestBody String json) {


        try {

            ProductDto[] produkty = productsModel.getProducts(json);

            String filename = service.getReceiptFile(produkty);


            FileInputStream receiptFile = new FileInputStream(filename);
            byte[] receipt = receiptFile.readAllBytes();
            receiptFile.close();

            return new ResponseEntity<>(receipt, HttpStatus.OK);
        }

        catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
